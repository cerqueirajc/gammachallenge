

# README

Nesta pasta encontram-se arquivos .csv com informações sobre escolas, redes de ensino, municípios  e o IDEB para anos iniciais e finais para escolas, municípios e estados.

No arquivo _filenames.txt_ encontram-se links para baixar dados comprimidos contendo informações sobre escolas, alunos, docentes e matrículas.

## Baixando e descomprimindo os arquivos

Para ajudá-los a baixar e descomprimir os arquivos preparamos duas funções em python:

```python
import os
import requests
import gzip

def download_data(link, output_dir):
    request = requests.get(link)
    content = request.content
    
    filename = link.split('/')[-1]

    with open(os.path.join(output_dir, filename.replace('csv', 'zip')), 'wb') as f:
        f.write(content)
    
    print(link)
    
    
def decompress_file(filename):
    with gzip.open(filename, 'rb') as f:
        content = f.read()
    
    decoded = content.decode('UTF-8')
    csv_fn = filename.replace('zip', 'csv')
    with open(csv_fn, 'w', encoding='utf-8') as f:
        f.write(decoded)
        
    print(csv_fn)

    
def create_data_folders(filenames):
    split_key = "https://storage.googleapis.com/qedu-dados/"
    keys = filenames.str.split(split_key).str[-1]
    keys = keys.str.split("20").str[0].unique()

    if os.path.exists("./fetch_data/"):
        print("Diretório fetch_data já existe")
    else:
        os.mkdir("./fetch_data/")
        for key in keys:
            os.mkdir("./fetch_data/" + key)

            
def fetch_all_data(filenames_filepath):
    filenames = pd.read_csv(filenames_filepath, header = None)[0]
    create_data_folders(filenames)
    split_key = "https://storage.googleapis.com/qedu-dados/"

    for filename in filenames:
        folder = filename.split(split_key)[-1].split('20')[0]
        path = "./fetch_data/" + folder + "/"
        downloaded_file = filename.split(split_key)[-1]

        download_data(filename, path)

    
def decompress_all_data(filenames_filepath):
    filenames = pd.read_csv(filenames_filepath, header = None)[0]
    split_key = "https://storage.googleapis.com/qedu-dados/"
    
    for filename in filenames:
        folder = filename.split(split_key)[-1].split('20')[0]
        path = "./fetch_data/" + folder + "/"
        downloaded_file = filename.split(split_key)[-1]
        
        try:
            decompress_file(path + downloaded_file.replace('csv', 'zip'))
        except:
            print('file not found - ' + downloaded_file.replace('csv', 'zip'))
            continue
```

Use a função ```download_data```  para baixar um dos arquivos comprimidos e salvar no diretório de sua escolha:

```python
# Exemplo
download_data("https://storage.googleapis.com/qedu-dados/alunos20150101000000000000.csv", ".")
```

Como output teremos o arquivo ```alunos20150101000000000000.zip```  na mesma pasta onde o script foi rodado. Para descomprimir o arquivo podemos usar a função ```decompress_file```:

```python
# Exemplo
decompress_file("alunos20150101000000000000.zip", '.')
```

Como output teremos o arquivo ```alunos20150101000000000000.csv``` , agora descomprimido, na mesma pasta onde o script foi rodado.

Use a função ```fetch_all_data``` para baixar todos os arquivos contidos em filename.txt, em formato comprimido. Eles serão armazenados dentro da pasta _fetch_data_ nos diretórios referentes ao tipo de dado (aluno, docente, etc.):

```python
# Exemplo
fetch_all_data("filenames.txt")
```

Use a função ```decompress_all_data``` para descomprimir os arquivos baixados. Eles serão armazenados nas mesmas pastas dos .zip:

```python
# Exemplo
decompress_all_data("filenames.txt")
```

## Dicionário de dados

Nos links abaixo temos as descrições dos campos das tabelas do INEP:

https://storage.googleapis.com/qedu-dados/schemas/alunos_schema.json

https://storage.googleapis.com/qedu-dados/schemas/docencias_schema.json

https://storage.googleapis.com/qedu-dados/schemas/docentes_schema.json

https://storage.googleapis.com/qedu-dados/schemas/escolas_schema.json

https://storage.googleapis.com/qedu-dados/schemas/matriculas_ensino_integral_schema.json

https://storage.googleapis.com/qedu-dados/schemas/matriculas_schema.json

https://storage.googleapis.com/qedu-dados/schemas/municipios_schema.json

https://storage.googleapis.com/qedu-dados/schemas/redes_schema.json