import pandas as pd
import numpy as np
import json

'''
Helper function to get column name and respective type from the
schema files.

Example: escolas_schema = json.load( open(os.path.join(schemas,
			'escolas_schema.json')) )
		escolas_2007 = pd.read_csv(os.path.join(escolas, 'escolas20070101.csv'),
			dtype = zip_dtype(escolas_schema))
'''
def zip_dtype(json_path):
	arr = json.load( open(json_path, encoding='utf-8') )
	typeMap = {
		'string': str,
		'boolean': bool,
		'integer': np.float64,
		'float': np.float64
	}
	return { arr[i]['name']: typeMap[arr[i]['type']] for i in range(len(arr)) }


def is_constant(x):
   try:
       x = x.values
   except AttributeError:
       pass

   return ((x == x[0]).all() or pd.Series(x).isnull().all())


"""# TODO remove this and not use inplace ?
def filter_ideb(ideb):
    cols_type_str = ['Co_UF', 'Nome_Municipio', 'Nome_Escola', 'Rede']
    for col in ideb:
        if col in cols_type_str:
            ideb[col] = ideb[col].astype('str')
        else:
            ideb[col] = ideb[col].astype(str).str.replace(',', '.').str.replace('*', '').str.replace('ND','NaN').astype(np.float64)
    return"""


def filter_ideb(ideb):
    cols_type_str = ['Co_UF', 'Nome_Municipio', 'Nome_Escola', 'Rede']
    df = ideb.copy()
    for col in df:
        if col in cols_type_str:
            df[col] = ideb[col].astype('str')
        else:
            df[col] = ideb[col].astype(str).str.replace(',', '.').str.replace('*', '').str.replace('ND','NaN').astype(np.float64)
    return df


def load_escolas(year):
    escolas = pd.read_csv('../data/escolas/escolas' + str(year) + '0101.csv',
                               dtype=zip_dtype('../schemas/escolas_schema.json'))
    escolas['cod_escola_inep'] = escolas['cod_escola_inep'].astype('int')
    constant_columns = escolas.apply(lambda x: is_constant(x))
    escolas = escolas.drop(constant_columns[constant_columns==True].index, axis=1)
    escolas = escolas[escolas['tp_rede'].str.lower() != 'privada']
    escolas = escolas[escolas['tp_rede_publica'] == True]
    
    return escolas


def clean_escola(escolas):
    to_remove = []
    to_int = []    
    for col in escolas.columns:
        col_type = escolas[col].dtype

        if col_type == 'object':
            to_remove.append(col)
        elif col_type == 'bool':
            to_int.append(col)

    escolas = escolas.drop(to_remove,axis=1)
    escolas[to_int] = escolas[to_int].astype(int)
    
    return escolas


def bool_to_int(df):
    to_int = []
    
    for col in df.columns:
        col_type = df[col].dtype
        if col_type == 'bool':
            to_int.append(col)
            
    df[to_int] = df[to_int].astype(int)
    
    return df


def load_docencias(year):
    docencias = pd.read_csv('../data/docencias/docencias' + str(year) + '0101000000000000.csv',
                               dtype=zip_dtype('../schemas/docencias_schema.json'))
    docencias['cod_escola_inep'] = docencias['cod_escola_inep'].astype('int')
    constant_columns = docencias.apply(lambda x: is_constant(x))
    docencias = docencias.drop(constant_columns[constant_columns == True].index, axis=1)
    
    return docencias


# TODO remove this
def load_ideb_escolas(iniciais): # iniciais: True or False
    if iniciais:
        age = 'iniciais'
    else:
        age = 'finais'
    ideb = pd.read_csv('../data/ideb/ideb_escolas_anos'+age+'2005_2017.csv', encoding='latin1', na_values=['-', 'ND','ND**'])
    filter_ideb(ideb)
    ideb['Cod_Municipio_Completo'] = ideb['Cod_Municipio_Completo'].astype(np.float64)
    return ideb


def load_ideb(agregacao, etapa):
    assert agregacao in ['escolas', 'municipios', 'uf_regioes'], "Nível inválido de agregação."
    assert etapa in ['finais', 'iniciais'], "Etapa de ensino inválida. Escolha 'finais' ou 'iniciais'."
    
    ideb = pd.read_csv(f"../data/ideb/ideb_{agregacao}_anos{etapa}2005_2017.csv",
                       encoding='latin1', na_values=['-', 'ND','ND**'])
    ideb = filter_ideb(ideb)
    ideb['Cod_Municipio_Completo'] = ideb['Cod_Municipio_Completo'].astype(np.float64)
    
    return ideb


def merge_ideb_to_frame(year, frame, ideb_iniciais, ideb_finais):
    if year < 2017 and year >= 2007:
        cols = ideb_iniciais.columns[ideb_iniciais.columns.str.match(r'Cod_Escola_Completo'+'|Ideb'+str(year)+'|Ideb'+str(year+2))]
        ideb_iniciais_ano = ideb_iniciais[cols]
        ideb_iniciais_ano['diff_'+str(year)] = ideb_iniciais_ano['Ideb'+str(year+2)] - ideb_iniciais_ano['Ideb'+str(year)]
    
        cols = ideb_finais.columns[ideb_finais.columns.str.match(r'Cod_Escola_Completo'+'|Ideb'+str(year)+'|Ideb'+str(year+2))]
        ideb_finais_ano = ideb_finais[cols]
        ideb_finais_ano['diff_'+str(year)] = ideb_finais_ano['Ideb'+str(year+2)] - ideb_finais_ano['Ideb'+str(year)]

    elif year == 2017:
        cols = ideb_iniciais.columns[ideb_iniciais.columns.str.match(r'Cod_Escola_Completo'+'|Ideb'+str(year))]
        ideb_iniciais_ano = ideb_iniciais[cols]
        
        cols = ideb_finais.columns[ideb_finais.columns.str.match(r'Cod_Escola_Completo'+'|Ideb'+str(year))]
        ideb_finais_ano = ideb_finais[cols]

    else:
        return None
    
    frame_merge = frame.merge(ideb_iniciais_ano,
                              left_on='cod_escola_inep',
                              right_on='Cod_Escola_Completo',
                              how='left',
                              suffixes=['','']).drop('Cod_Escola_Completo', axis=1)
    frame_merge = frame_merge.merge(ideb_finais_ano,
                                    left_on='cod_escola_inep',
                                    right_on='Cod_Escola_Completo',
                                    how='left',
                                    suffixes=['','_final']).drop('Cod_Escola_Completo', axis=1)

    return frame_merge
