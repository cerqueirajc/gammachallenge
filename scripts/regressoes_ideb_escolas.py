import os
import argparse

import boto
import pandas as pd
from matplotlib import pyplot as plt
from tqdm import tqdm
from sklearn.linear_model import Ridge
from sklearn import preprocessing
from sklearn import model_selection
from sklearn.ensemble import RandomForestRegressor

from utils import exploratory as expl


parser = argparse.ArgumentParser()
parser.add_argument('--ano', type=int)
parser.add_argument('--root_folder', type=str)
parser.add_argument('--key_id', type=str)
parser.add_argument('--secret_key', type=str)


if __name__ == '__main__':

    opts = parser.parse_args()

    ano = opts.ano
    ano_seguinte = ano + 2
    root_folder = opts.root_folder

    # Get S3 bucket connection
    AWS_ACCESS_KEY_ID = opts.key_id
    AWS_SECRET_ACCESS_KEY = opts.secret_key
    bucket_name = 'bcg-gamma-challenge'
    s3_conn = boto.connect_s3(AWS_ACCESS_KEY_ID,
                              AWS_SECRET_ACCESS_KEY)
    bucket = s3_conn.get_bucket(bucket_name)

    # Paths to data files
    data_base = os.path.join(root_folder, 'data')
    escolas_path = os.path.join(data_base, 'escolas')
    docencias_path = os.path.join(data_base, 'docencias')
    ideb_path = os.path.join(data_base, 'ideb')
    schemas = os.path.join(root_folder, 'schemas')
    images = os.path.join(root_folder, 'images')
    logs = os.path.join(root_folder, 'logs')
    s3_images = 'gamma-challenge/data/images'
    s3_logs = 'gamma-challenge/data/logs'

    # Carregando dados de escolas
    escolas = expl.load_escolas(ano, escolas_path, schemas)

    # Relações entre quantidades de alunos e professores, funcionários, salas...

    escolas['alunos_por_prof'] = escolas['num_estudantes'] / escolas['num_professores']
    escolas['alunos_por_turma'] = escolas['num_estudantes'] / escolas['num_turmas']

    escolas['turmas_por_professor'] = escolas['num_turmas'] / escolas['num_professores']
    escolas['percentual_professores_em_regencia'] = (escolas['num_professores_em_regencia']
                                                     / escolas['num_professores'])

    escolas['funcionarios_por_aluno'] = escolas['num_funcionarios'] / escolas['num_estudantes']
    escolas['funcionarios_por_professor'] = escolas['num_funcionarios'] / escolas['num_professores']

    escolas['num_alunos_por_sala_existente'] = escolas['num_estudantes'] / escolas['num_salas_existentes']
    escolas['num_alunos_por_sala_utilizada'] = escolas['num_estudantes'] / escolas['num_salas_utilizadas']

    escolas['percentual_evasao_abstencao_alunos'] = 1 - escolas['num_estudantes'] / escolas['num_matriculas']

    # Ligando as informações de docência às escolas
    docencias = expl.load_docencias(ano, docencias_path, schemas)

    escolas_docencias = escolas.merge(docencias, on='cod_escola_inep')

    del escolas

    escolas_docencias = escolas_docencias.merge(docencias.groupby('cod_escola_inep',
                                                                  as_index=False)['num_disciplinas_ensina']
                                                         .sum(), on='cod_escola_inep')

    # num_disciplinas_ensina: média do número de disciplinas dadas por professor por escola
    escolas_docencias = escolas_docencias.merge(docencias.groupby('cod_escola_inep',
                                                                  as_index=False)['num_disciplinas_ensina'].mean(),
                                                on='cod_escola_inep')

    # quantidade de professores que possuem ensino superior por escola
    escolas_docencias = escolas_docencias.merge(docencias.groupby('cod_escola_inep',
                                                                  as_index=False)['tem_ensino_superior'].sum(),
                                                on='cod_escola_inep')

    constant_columns = escolas_docencias.apply(lambda x: expl.is_constant(x))
    escolas_docencias = escolas_docencias.drop(constant_columns[constant_columns == True].index, axis=1)

    # Carregando dados de IDEBs e ligando aos de escola
    ideb_iniciais = expl.load_ideb('escolas', 'iniciais', ideb_path)
    ideb_finais = expl.load_ideb('escolas', 'finais', ideb_path)

    ideb_escolas = expl.merge_ideb_to_frame(ano, escolas_docencias, ideb_iniciais, ideb_finais)

    del escolas_docencias

    # Selecionando quais colunas do dataframe resultante usar.
    # Em geral, nao sao usadas as que entraram na criacao de features e as que sao
    # totais numericos ilimitados
    select_columns = [column for column in ideb_escolas.columns if not (
        column.startswith('num_estudantes')
        or column.startswith('ideb_')
        or column.startswith('num_matriculas')
        or column.startswith('num_funcionarios')
        or column.startswith('num_professores')
        or column.startswith('num_salas')
        or column.startswith('Nota')
        or column.startswith('Projecao')
        or column.startswith('etapa')
        or column.startswith('Indicador')
        or column.startswith('Unnamed')
        or column.endswith('_x')
        or column.endswith('_y')
        or column.endswith('_desc')
    )]

    drop_columns = [
        'nome_escola',
        'cod_ibge_uf',
        'mesoregiao',
        'microregiao',
        'in_dependencias_outras',
        'id_turma',
        'num_turmas',
        'cod_escola_inep',
        'co_pessoa_fisica',
        'co_lingua_indigena',
        'tp_rede_cod',
        'tp_rede_nome',
        'tp_etapa_ensino',
    ]

    # Identificando as colunas que contêm dados de IDEBs
    if 2007 <= ano < 2017:
        ideb_cols = [f"Ideb{ano}", f"Ideb{ano}_final",
                     f"Ideb{ano_seguinte}", f"Ideb{ano_seguinte}_final",
                     f"diff_{ano}", f"diff_{ano}_final"]
    elif ano == 2017:
        ideb_cols = [f"Ideb{ano}", f"Ideb{ano}_final"]

    esc_ideb_clean = ideb_escolas[select_columns]

    del ideb_escolas

    esc_ideb_clean = esc_ideb_clean.drop(drop_columns, axis=1)
    esc_ideb_clean = esc_ideb_clean.dropna(subset=ideb_cols)

    constant_columns = esc_ideb_clean.apply(lambda x: expl.is_constant(x))
    esc_ideb_clean = esc_ideb_clean.drop(constant_columns[constant_columns == True].index, axis=1)

    esc_ideb_clean['in_internet'] = esc_ideb_clean['in_internet'].astype('bool').fillna(False)

    # Definicao dos possiveis targets para modelos de regressão
    targets = {
        'total_iniciais': esc_ideb_clean.reset_index()[f"Ideb{ano}"],
        'total_finais': esc_ideb_clean.reset_index()[f"Ideb{ano}_final"],
        'diff_iniciais': esc_ideb_clean.reset_index()[f"diff_{ano}"],
        'diff_finais': esc_ideb_clean.reset_index()[f"diff_{ano}_final"]
    } if ano != 2017 else {
        'total_iniciais': esc_ideb_clean.reset_index()[f"Ideb{ano}"],
        'total_finais': esc_ideb_clean.reset_index()[f"Ideb{ano}_final"],
    }

    # Evitar ter o objetivo nos dados de entrada
    esc_ideb_clean = esc_ideb_clean.drop(ideb_cols, axis=1)

    esc_ideb_clean = expl.bool_to_int(esc_ideb_clean)

    # Colunas categóricas, para as quais precisamos fazer One-Hot Encoding
    str_cols = [col for col, dtype in zip(esc_ideb_clean.columns, esc_ideb_clean.dtypes) if dtype == 'object']
    df_enc = pd.get_dummies(esc_ideb_clean, columns=str_cols)

    constant_columns = df_enc.apply(lambda x: expl.is_constant(x))
    df_enc = df_enc.drop(constant_columns[constant_columns == True].index, axis=1)
    df_enc = df_enc.fillna(df_enc.mean())

    # Colunas sobre as quais sera aplicado escalonamento
    to_normalize = [x[0] for x in (df_enc.dtypes == 'float64').items() if x[1]]
    scalers = {k: preprocessing.RobustScaler().fit(df_enc[k].values.reshape(-1, 1)) for k in to_normalize}
    for column in to_normalize:
        df_enc[column] = scalers[column].transform(df_enc[column].values.reshape(-1, 1))

    for option, target in tqdm(targets.items()):
        # Divisão entre treino e teste
        X_train, X_test, y_train, y_test = model_selection.train_test_split(df_enc, target, test_size=0.2)

        # Regressão Linear: Quais variáveis explicam melhor o objetivo?
        # Utilizamos uma regressão linear penalizada com norma 2.
        ridge_reg = Ridge().fit(X_train, y_train)
        pred = ridge_reg.predict(X_test)

        plt.title("Regressão Linear para {}: Previsões em conjunto de testes".format(option))
        plt.xlabel("Índice da escola")
        plt.ylabel("Nota" if 'total' in option else "Variação de Nota")
        plt.plot(y_test.reset_index(drop=True))
        plt.plot(pred)
        plt.savefig(os.path.join(images, str(ano), 'ridge_' + option))
        expl.send_file_to_bucket(bucket, '/'.join([s3_images, str(ano)]), os.path.join(images, ano),
                                 'ridge_' + option + '.png')

        ridge_features = [(y, x) for x, y in sorted(zip(ridge_reg.coef_, X_train.columns), reverse=True)]
        with open(os.path.join(logs, str(ano), 'ridge_features_' + option), 'w') as f:
            f.write(str(ridge_features))
        expl.send_file_to_bucket(bucket, '/'.join([s3_logs, str(ano)]), os.path.join(logs, ano),
                                 'ridge_features_' + option)

        ridge_features_mean_weighted = (df_enc.mean() * ridge_reg.coef_).sort_values(ascending=False)
        with open(os.path.join(logs, str(ano), 'ridge_weighted_features_' + option), 'w') as f:
            f.write(str(ridge_features_mean_weighted))
        expl.send_file_to_bucket(bucket, '/'.join([s3_logs, str(ano)]), os.path.join(logs, ano),
                                 'ridge_weighted_features_' + option)

        # Tentando predizer o IDEB ou a sua variação
        rf_reg = RandomForestRegressor(max_depth=20, random_state=0, n_estimators=100, n_jobs=-1)
        rf_reg.fit(X_train, y_train)
        score = rf_reg.score(X_test, y_test)

        pred_rf = rf_reg.predict(X_test)

        plt.title("Regressão com Random Forest para ", option)
        plt.suptitle("Score = {:.3f}".format(score))
        plt.xlabel("Índice da escola")
        plt.ylabel("Nota" if 'total' in option else "Variação de Nota")
        plt.plot(y_test.reset_index(drop=True))
        plt.plot(pred_rf)
        plt.savefig(os.path.join(images, str(ano), 'reg_forest_' + option))
        expl.send_file_to_bucket(bucket, '/'.join([s3_images, str(ano)]), os.path.join(images, ano),
                                 'reg_forest_' + option + '.png')

        feature_importances = [(y, x) for x, y in sorted(zip(rf_reg.feature_importances_, X_test.columns),
                                                         reverse=True)]
        with open(os.path.join(logs, str(ano), 'forest_importances_' + option), 'w') as f:
            f.write(str(feature_importances))
        expl.send_file_to_bucket(bucket, '/'.join([s3_logs, str(ano)]), os.path.join(logs, ano),
                                 'forest_importances_' + option)
