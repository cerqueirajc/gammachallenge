"""Helper functions and data loading functions."""
import os
import json

import numpy as np
import pandas as pd
from boto.s3.key import Key


def zip_dtype(json_path):
    """Parse schema and return column name and dtype only.

    Helper function to get column name and respective type from the
    schema files.

    Example: escolas_schema = json.load( open(os.path.join(schemas, 'escolas_schema.json')) )
             escolas_2007 = pd.read_csv(os.path.join(escolas, 'escolas20070101.csv'),
             dtype = zip_dtype(escolas_schema))

    """
    arr = json.load( open(json_path, encoding='utf-8') )
    type_map = {
        'string': str,
        'boolean': bool,
        'integer': np.float64,
        'float': np.float64
    }
    return {arr[i]['name']: type_map[arr[i]['type']] for i in range(len(arr))}


def is_constant(x):
    """Verify if an array-like is constant."""
    try:
        x = x.values
    except AttributeError:
        pass

    return (x == x[0]).all() or pd.Series(x).isnull().all()


def filter_ideb(ideb):
    """Remove junk values and correct dtypes in IDEB frame."""
    cols_type_str = ['Co_UF', 'Nome_Municipio', 'Nome_Escola', 'Rede']
    df = ideb.copy()
    for col in df:
        if col in cols_type_str:
            df[col] = ideb[col].astype('str')
        else:
            df[col] = (ideb[col].astype(str)
                                .str.replace(',', '.')
                                .str.replace('*', '')
                                .str.replace('ND', 'NaN')
                                .astype(np.float64))
    return df


def load_escolas(year, data_path, schemas_path):
    """Load data for public schools. Drops constant columns."""
    escolas = pd.read_csv(os.path.join(data_path, 'escolas' + str(year) + '0101.csv'),
                          dtype=zip_dtype(os.path.join(schemas_path, 'escolas_schema.json')))
    escolas['cod_escola_inep'] = escolas['cod_escola_inep'].astype('int')
    constant_columns = escolas.apply(lambda x: is_constant(x))
    escolas = escolas.drop(constant_columns[constant_columns == True].index, axis=1)
    escolas = escolas[escolas['tp_rede'].str.lower() != 'privada']
    escolas = escolas[escolas['tp_rede_publica'] == True]
    
    return escolas


def clean_escola(escolas):
    to_remove = []
    to_int = []    
    for col in escolas.columns:
        col_type = escolas[col].dtype

        if col_type == 'object':
            to_remove.append(col)
        elif col_type == 'bool':
            to_int.append(col)

    escolas = escolas.drop(to_remove,axis=1)
    escolas[to_int] = escolas[to_int].astype(int)
    
    return escolas


def bool_to_int(df):
    """Convert boolean columns to integers."""
    to_int = []
    
    for col in df.columns:
        col_type = df[col].dtype
        if col_type == 'bool':
            to_int.append(col)
            
    df[to_int] = df[to_int].astype(int)
    
    return df


def load_docencias(year, data_path, schemas_path):
    print(list(os.walk(data_path)))
    docencias_files = list(os.walk(data_path))[0][2]
    docencias_files = [file for file in docencias_files if str(year) in file]
    print(docencias_files)

    docencias = pd.DataFrame()
    for file in docencias_files:
        docencias = docencias.append(pd.read_csv(os.path.join(data_path, file),
                                                 dtype=zip_dtype(os.path.join(schemas_path,
                                                                              'docencias_schema.json'))).sample(frac=0.2))

    docencias['cod_escola_inep'] = docencias['cod_escola_inep'].astype('int')
    constant_columns = docencias.apply(lambda x: is_constant(x))
    docencias = docencias.drop(constant_columns[constant_columns == True].index, axis=1)
    
    return docencias


def load_ideb(agregacao, etapa, data_path):
    assert agregacao in ['escolas', 'municipios', 'uf_regioes'], "Nível inválido de agregação."
    assert etapa in ['finais', 'iniciais'], "Etapa de ensino inválida. Escolha 'finais' ou 'iniciais'."
    
    ideb = pd.read_csv(os.path.join(data_path, f"ideb_{agregacao}_anos{etapa}2005_2017.csv"),
                       encoding='latin1', na_values=['-', 'ND','ND**'])
    ideb = filter_ideb(ideb)
    ideb['Cod_Municipio_Completo'] = ideb['Cod_Municipio_Completo'].astype(np.float64)
    
    return ideb


def merge_ideb_to_frame(year, frame, ideb_iniciais, ideb_finais):
    if 2007 <= year < 2017:
        cols = ideb_iniciais.columns[ideb_iniciais.columns.str.match(r'Cod_Escola_Completo'+'|Ideb'+str(year)+'|Ideb'+str(year+2))]
        ideb_iniciais_ano = ideb_iniciais[cols]
        ideb_iniciais_ano['diff_'+str(year)] = ideb_iniciais_ano['Ideb'+str(year+2)] - ideb_iniciais_ano['Ideb'+str(year)]
    
        cols = ideb_finais.columns[ideb_finais.columns.str.match(r'Cod_Escola_Completo'+'|Ideb'+str(year)+'|Ideb'+str(year+2))]
        ideb_finais_ano = ideb_finais[cols]
        ideb_finais_ano['diff_'+str(year)] = ideb_finais_ano['Ideb'+str(year+2)] - ideb_finais_ano['Ideb'+str(year)]

    elif year == 2017:
        cols = ideb_iniciais.columns[ideb_iniciais.columns.str.match(r'Cod_Escola_Completo'+'|Ideb'+str(year))]
        ideb_iniciais_ano = ideb_iniciais[cols]
        
        cols = ideb_finais.columns[ideb_finais.columns.str.match(r'Cod_Escola_Completo'+'|Ideb'+str(year))]
        ideb_finais_ano = ideb_finais[cols]

    else:
        return None
    
    frame_merge = frame.merge(ideb_iniciais_ano,
                              left_on='cod_escola_inep',
                              right_on='Cod_Escola_Completo',
                              how='left',
                              suffixes=['', '']).drop('Cod_Escola_Completo', axis=1)
    frame_merge = frame_merge.merge(ideb_finais_ano,
                                    left_on='cod_escola_inep',
                                    right_on='Cod_Escola_Completo',
                                    how='left',
                                    suffixes=['', '_final']).drop('Cod_Escola_Completo', axis=1)

    return frame_merge


def send_file_to_bucket(bucket, s3_path, fs_path, filename):
    k = Key(bucket, '/'.join([s3_path, filename]))
    k.set_contents_from_filename('/'.join([fs_path, filename]))
    print('Uploaded file {} to path {}.'.format(filename, s3_path))
