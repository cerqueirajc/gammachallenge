import logging
from argparse import ArgumentParser
from subprocess import Popen


parser = ArgumentParser()
parser.add_argument('--key_id', type=str)
parser.add_argument('--secret_key', type=str)

if __name__ == '__main__':
    FORMAT = '%(name)s %(levelname)s: %(asctime)-15s %(message)s'
    logging.basicConfig(format=FORMAT)
    logger = logging.getLogger('regressions_ideb')
    opts = parser.parse_args()
    AWS_ACCESS_KEY_ID = opts.key_id
    AWS_SECRET_ACCESS_KEY = opts.secret_key

    years = [2017, 2015, 2013, 2011, 2009, 2007]
    for year in years:
        with Popen(['python', 'regressoes_ideb_escolas.py',
                    '--ano=' + str(year), '--root_folder=../',
                    '--key_id=' + AWS_ACCESS_KEY_ID,
                    '--secret_key=' + AWS_SECRET_ACCESS_KEY]) as proc:
            outs, errs = proc.communicate()
            logger.info(outs)
            logger.error(errs)
