import os
import pandas as pd
#import xarray as xr
#import seaborn as sns
#from matplotlib import pyplot as plt
#from tqdm import tqdm
import numpy as np
import json

root_folder = os.path.relpath('../')
escolas = os.path.join(root_folder, 'data/escolas')
ideb = os.path.join(root_folder, 'data/ideb')
municipios = os.path.join(root_folder, 'data/municipios')
redes = os.path.join(root_folder, 'data/redes')
schemas = os.path.join(root_folder, 'schemas/')

def zip_dtype(arr):
	typeMap = {
		'string': str,
		'boolean': bool,
		'integer': np.float64,
		'float': np.float64
	}
	return { arr[i]['name']: typeMap[arr[i]['type']] for i in range(len(arr)) }

escolasSchema = json.load( open(os.path.join(schemas, 'escolas_schema.json')) )
municipiosSchema = json.load( open(os.path.join(schemas, 'municipios_schema.json')) )
redesSchema = json.load( open(os.path.join(schemas, 'redes_schema.json')) )

escolas_2007 = pd.read_csv(os.path.join(escolas, 'escolas20070101.csv'), dtype = zip_dtype(escolasSchema))
#municipios_2015 = pd.read_csv(os.path.join(municipios, 'municipios20150101.csv'))
#redes_2015 = pd.read_csv(os.path.join(redes, 'redes20150101.csv'))
#ideb_escolas_anosfinais2005_2017 = pd.read_csv(os.path.join(municipios, 'ideb_escolas_anosfinais2005_2017.csv'))
